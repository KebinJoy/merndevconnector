import React, { Fragment, useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { editExperience, getCurrentProfile } from "../../actions/profile";
import Spinner from "../layout/Spinner";

const EditExperience = ({
  editExperience,
  history,
  profile: { profile, loading },
  auth,
  getCurrentProfile,
  match,
}) => {
  const [formData, setFormData] = useState({
    company: "",
    title: "",
    location: "",
    from: "",
    to: "",
    current: false,
    description: "",
  });
  const { company, title, location, from, to, current, description } = formData;

  const [toDateDisbled, toggleDisabled] = useState(false);

  useEffect(() => {
    getCurrentProfile();
    if (profile != null) {
      const oldExp =
        profile.experience[
          profile.experience.map((exp) => exp._id).indexOf(match.params.id)
        ];

      setFormData({
        company: loading || !oldExp.company ? "" : oldExp.company,
        title: loading || !oldExp.title ? "" : oldExp.title,
        location: loading || !oldExp.location ? "" : oldExp.location,
        from: loading || !oldExp.from ? "" : oldExp.from.slice(0, 10),
        to: loading || !oldExp.to ? "" : oldExp.to.slice(0, 10),
        current: loading || !oldExp.current ? false : true,
        description: loading || !oldExp.description ? "" : oldExp.description,
      });
      toggleDisabled(oldExp.current);
    }
  }, [getCurrentProfile, loading]);

  const onChange = (e) =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  return (
    <Fragment>
      {profile === null || loading ? (
        <Spinner />
      ) : (
        <Fragment>
          {auth.isAuthenticated &&
          auth.loading === false &&
          auth.user._id === profile.user._id ? (
            <Fragment>
              {" "}
              <h1 className="large text-primary">Edit An Experience</h1>
              <p className="lead">
                <i className="fas fa-code-branch"></i> Edit your position
              </p>
              <small>* = required field</small>
              <form
                className="form"
                onSubmit={(e) => {
                  e.preventDefault();
                  editExperience(formData, history, match.params.id);
                }}
              >
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="* Job Title"
                    name="title"
                    value={title}
                    onChange={(e) => onChange(e)}
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="* Company"
                    name="company"
                    value={company}
                    onChange={(e) => onChange(e)}
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Location"
                    name="location"
                    value={location}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div className="form-group">
                  <h4>From Date</h4>
                  <input
                    type="date"
                    name="from"
                    value={from}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div className="form-group">
                  <p>
                    <input
                      type="checkbox"
                      name="current"
                      checked={current}
                      value={current}
                      onChange={(e) => {
                        setFormData({
                          ...formData,
                          current: !current,
                          to: "",
                        });
                        toggleDisabled(!toDateDisbled);
                      }}
                    />{" "}
                    Current Job
                  </p>
                </div>
                <div className="form-group">
                  <h4>To Date</h4>
                  <input
                    type="date"
                    name="to"
                    value={to}
                    onChange={(e) => onChange(e)}
                    disabled={toDateDisbled ? "disabled" : ""}
                  />
                </div>
                <div className="form-group">
                  <textarea
                    name="description"
                    cols="30"
                    rows="5"
                    placeholder="Job Description"
                    value={description}
                    onChange={(e) => onChange(e)}
                  ></textarea>
                </div>
                <input type="submit" className="btn btn-primary my-1" />
              </form>
            </Fragment>
          ) : (
            <Fragment>You are not authourized</Fragment>
          )}
          <Link className="btn btn-light my-1" to="/dashboard">
            Go Back
          </Link>
        </Fragment>
      )}
    </Fragment>
  );
};

EditExperience.propTypes = {
  editExperience: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth,
});

export default connect(mapStateToProps, {
  editExperience,
  getCurrentProfile,
})(withRouter(EditExperience));
