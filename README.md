## Running the app

To run the app, first clone this repo to your system and do the following.

You need Node v16 to run this app. Use NVM to install Node 16.15.0.

1. Do `npm i` on the root of the project
2. Do `npm run dev` and visit `http://localhost:3000/`

This message will self destruct in Five seconds
